﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing    ;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ConsensoInformato2
{
    public partial class FormMain : Form
    {
        ConsensiController _controller = null;
        int _currentId = -1;
        Font _detailsFont = new Font("Arial", 10);

        public FormMain()
        {
            InitializeComponent();

            lblPaziente.Text += Common.Cognome + " " + Common.Nome;
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        internal void SetController(ConsensiController controller)
        {
            if (_controller != null && _controller != controller) {
                _controller.TemplatesLoaded -= OnTemplatesLoaded;
                _controller.SelectionChanged -= OnSelectionChanged;
            }

            _controller = controller;

            if (_controller != null) {
                _controller.TemplatesLoaded += OnTemplatesLoaded;
                _controller.SelectionChanged += OnSelectionChanged;
            }
        }

        private void cmdGenerate_Click(object sender, EventArgs e)
        {
            if (lsbSelezionati.Items.Count > 0) {
                _controller.GenerateDocuments();
            }
        }

        #region Eventi da controller
        private void OnTemplatesLoaded(List<ConsensoTemplate> templates)
        {
            lsbModelli.Items.Clear();
            lsbSelezionati.ClearSelected();
            lsbModelli.DisplayMember = "Label";
            lsbSelezionati.DisplayMember = "Label";

            foreach (var item in templates) {
                var index = lsbModelli.Items.Add(new { Label = item.FileLabel, Id = item.Id } );
            }
        }

        private void OnSelectionChanged(bool isAdd, ConsensoTemplate template)
        {
            var item = new { Label = template.FileLabel, Id = template.Id };
            var itemId = template.Id;

            void InsertItem(ListBox lst)
            {
                var index = 0;

                foreach(var listItem in lst.Items) {
                    var listItemId = (int)((long)listItem.GetType().GetProperty("Id").GetValue(listItem, null)); 
                    if (listItemId > itemId) {
                        lst.Items.Insert(index, item);
                        index = -1;
                        break;
                    }
                    index++;
                }

                if (index >= 0) {
                    lst.Items.Add(item);
                }
            }

            if (isAdd) {
                InsertItem(lsbSelezionati);
                lsbModelli.Items.Remove(item);
            }
            else {
                InsertItem(lsbModelli);
                lsbSelezionati.Items.Remove(item);
            }
        }

        #endregion Eventi da controller

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            if (lsbModelli.Items.Count == 0 || lsbModelli.SelectedIndex < 0) return;

            var idx = lsbModelli.SelectedIndex;
            var item = lsbModelli.Items[idx];
            var id = (int)((long)item.GetType().GetProperty("Id").GetValue(item, null));
            _controller.SelectTemplate(id);
        }

        private void cmdRemove_Click(object sender, EventArgs e)
        {
            if (lsbSelezionati.Items.Count == 0 || lsbSelezionati.SelectedIndex < 0) return;

            var idx = lsbSelezionati.SelectedIndex;
            var item = lsbSelezionati.Items[idx];
            var id = (int)((long)item.GetType().GetProperty("Id").GetValue(item, null));
            _controller.DeselectTemplate(id);
            _currentId = -1;
            ClearDetailsTab();

            lsbSelezionati.SelectedIndex = (idx < lsbSelezionati.Items.Count) ? idx : idx - 1;
        }

        private void lsbSelezionati_SelectedIndexChanged(object sender, EventArgs e)
        {
            var idx = lsbSelezionati.SelectedIndex;

            _currentId = -1;

            ClearDetailsTab();

            if (idx >= 0) {
                var item = lsbSelezionati.Items[idx];
                var selectedId = (int)((long)item.GetType().GetProperty("Id").GetValue(item, null));
                var template = _controller.GetSelectedTemplate(selectedId);

                foreach(var sezione in template.TemplateSections) {
                    TextBox txt = new TextBox() {
                        Name = sezione.Label,
                        Text = sezione.Detail,
                        Multiline = true,
                        Font = _detailsFont,
                        Dock = DockStyle.Fill
                    };
                    txt.TextChanged += txtDettaglio_TextChanged;
                    var tabDettaglio = new TabPage() {
                        Font = _detailsFont,
                        Text = sezione.Label
                    };

                    tabDettaglio.Controls.Add(txt);
                    tabDettagli.TabPages.Add(tabDettaglio);
                }
                if (tabDettagli.TabPages.Count > 0) {
                    tabDettagli.SelectedTab = tabDettagli.TabPages[0];
                    tabDettagli.Enabled = true;
                }

                _currentId = selectedId;
            }
        }

        private void txtDettaglio_TextChanged(object sender, EventArgs e)
        {
            if (_currentId >= 0) {
                var template = _controller.GetSelectedTemplate(_currentId);

                if (template.TemplateSections.Count > 0) {
                    var txt = sender as TextBox;
                    var name = txt.Name;

                    foreach(var sezione in template.TemplateSections) {
                        if (sezione.Label == name) {
                            sezione.Detail = txt.Text;
                        }
                    }
                }
            }
        }

        private void rdbChiFirma_CheckedChanged(object sender, EventArgs e)
        {
            if (sender == rdbMadre) {
                _controller.SetChiFirma(EChiFirma.Madre);
            }
            else if (sender == rdbPadre) {
                _controller.SetChiFirma(EChiFirma.Padre);
            }
            else if (sender == rdbTutore) {
                _controller.SetChiFirma(EChiFirma.Tutore);
            }
            else {
                _controller.SetChiFirma(EChiFirma.Paziente);
            }
        }

        private void ClearDetailsTab()
        {
            foreach (TabPage tabDettaglio in tabDettagli.TabPages) {
                if (tabDettaglio.Controls[0] is TextBox) {
                    var txt = tabDettaglio.Controls[0] as TextBox;
                    txt.TextChanged -= txtDettaglio_TextChanged;
                }
                tabDettaglio.Controls.Clear();
            }
            tabDettagli.TabPages.Clear();
            tabDettagli.Enabled = false;
        }
    }
}
