﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO.Compression;
using System.IO;

namespace ConsensoInformato2
{
    public delegate void TemplatesLoadedHandler(List<ConsensoTemplate> templates);
    public delegate void TemplateSelectHandler(bool isAdd, ConsensoTemplate template);

    class ConsensiController
    {
        public event TemplatesLoadedHandler TemplatesLoaded = null;
        public event TemplateSelectHandler SelectionChanged = null;
        public event EventHandler DetailChanged;
        public event EventHandler DocumentsGenerated;

        protected ConsensiModel _model = null;

        protected Dictionary<string, string> _argumentsReplacement = null;

        public void StartUp()
        {
            _model = new ConsensiModel();
            _model.LoadTemplates();

            _argumentsReplacement = new Dictionary<string, string>() {
                {Common.LabelCognome, Common.Cognome },
                {Common.LabelNome, Common.Nome },
                {Common.LabelCodiceFiscale, Common.CodiceFiscale },
                {Common.LabelIndirizzo, Common.Indirizzo },
                {Common.LabelDataNascita, Common.DataNascita },
                {Common.LabelLuogoNascita, Common.LuogoNascita },
                {Common.LabelDottore, Common.Dottore }
            };

           SafeCallTemplatesLoaded(_model.ConsensoTemplates);
        }

        public void ShutDown()
        {
            _model = null;
        }

        #region Metodi

        public void SelectTemplate(int templateId)
        {
            var itemSelected = (from x in _model.ConsensoTemplates
                                where x.Id == templateId
                                select x).FirstOrDefault();
            if (itemSelected != null) {
                _model.InsertConsenso(_model.ConsensoSelected, itemSelected);
                _model.ConsensoTemplates.Remove(itemSelected);
                SafeCallSelectionChanged(true, itemSelected);
            }
        }


        public void DeselectTemplate(int templateId)
        {
            var itemSelected = (from x in _model.ConsensoSelected
                                where x.Id == templateId
                                select x).FirstOrDefault();
            if (itemSelected != null) {
                _model.InsertConsenso(_model.ConsensoTemplates, itemSelected);
                
                foreach(var section in itemSelected.TemplateSections) {
                    section.Detail = string.Empty;
                }
                _model.ConsensoSelected.Remove(itemSelected);
                SafeCallSelectionChanged(false, itemSelected);
            }
        }

        public void GenerateDocuments()
        {
            ClearTmpDirectory();

            foreach (var consenso in _model.ConsensoSelected) {
                ZipFile.ExtractToDirectory(
                    consenso.FileTemplatePath,
                    Common.TemporaryStore);
                var contentFilePath = Common.TemporaryStore + "/content.xml";
                var fileContent = File.ReadAllText(contentFilePath);

                //  sostituzione argomenti
                foreach(var kp in _argumentsReplacement) {
                    fileContent = fileContent.Replace(kp.Key, kp.Value);
                }

                foreach (var section in consenso.TemplateSections) {
                    fileContent = fileContent.Replace($"$${section.Label}$$", section.Detail);
                }

                fileContent = fileContent.Replace(Common.LabelChiFirma,
                    _model.ChiFirma != EChiFirma.Paziente ? _model.ChiFirma.ToString() : string.Empty);

                fileContent = fileContent.Replace(Common.LabelData, DateTime.Now.ToString("F"));

                File.Delete(contentFilePath);
                File.WriteAllText(contentFilePath, fileContent);

                var intermediatePath = $"{Common.PdfDestination}\\{consenso.FileLabel}.{Common.TemplateExtension}";
                if (File.Exists(intermediatePath)) {
                    File.Delete(intermediatePath);
                }
                ZipFile.CreateFromDirectory(
                    Common.TemporaryStore,
                    intermediatePath,
                    CompressionLevel.Optimal,
                    false);

                ExecuteConversionCommand(consenso, intermediatePath);

                File.Delete(intermediatePath);
                ClearTmpDirectory();
            }
        }

        private void ExecuteConversionCommand(ConsensoTemplate consenso, string intermediatePath)
        {
            var arguments = Common.TransformerCommandArguments
                .Replace("$$outdir$$", Common.PdfDestination)
                .Replace("$$sourcefilepath$$", intermediatePath);

            var info = new ProcessStartInfo() {
                CreateNoWindow = false,
                UseShellExecute = false,
                RedirectStandardOutput=true,
                RedirectStandardError=true,
                FileName = Common.TransformerCommand,
                Arguments = arguments
            };

            var proc = new Process() {
                StartInfo = info
            };

            proc.Start();

            var output = proc.StandardOutput.ReadToEnd();
            var error = proc.StandardError.ReadToEnd();
            proc.WaitForExit();
        }

        private void ClearTmpDirectory()
        {
            DirectoryInfo dirInfo = new DirectoryInfo(Common.TemporaryStore);

            foreach (var file in dirInfo.EnumerateFiles()) {
                file.Delete();
            }
            foreach (var dir in dirInfo.EnumerateDirectories()) {
                Directory.Delete(dir.FullName, true);
            }
        }

        public ConsensoTemplate GetSelectedTemplate(int templateId)
        {
            return (from x in _model.ConsensoSelected
                    where x.Id == templateId
                    select x).FirstOrDefault();
        }

        public void SetChiFirma(EChiFirma who)
        {
            _model.ChiFirma = who;
        }

        #endregion Metodi

        #region eventi

        private void SafeCallTemplatesLoaded(List<ConsensoTemplate> consensoTemplates)
        {
            if (TemplatesLoaded != null) {
                TemplatesLoaded(consensoTemplates);
            }
        }

        private void SafeCallSelectionChanged(bool isAdd, ConsensoTemplate template)
        {
            if (SelectionChanged != null) {
                SelectionChanged(isAdd, template);
            }
        }


        #endregion eventi

    }
}
