﻿namespace ConsensoInformato2
{
    partial class FormMain
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdGenerate = new System.Windows.Forms.Button();
            this.cmdClose = new System.Windows.Forms.Button();
            this.lsbModelli = new System.Windows.Forms.ListBox();
            this.lsbSelezionati = new System.Windows.Forms.ListBox();
            this.cmdAdd = new System.Windows.Forms.Button();
            this.cmdRemove = new System.Windows.Forms.Button();
            this.grbFirma = new System.Windows.Forms.GroupBox();
            this.rdbTutore = new System.Windows.Forms.RadioButton();
            this.rdbMadre = new System.Windows.Forms.RadioButton();
            this.rdbPadre = new System.Windows.Forms.RadioButton();
            this.rdbPaziente = new System.Windows.Forms.RadioButton();
            this.tabDettagli = new System.Windows.Forms.TabControl();
            this.lblPaziente = new System.Windows.Forms.Label();
            this.grbFirma.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdGenerate
            // 
            this.cmdGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdGenerate.Location = new System.Drawing.Point(887, 487);
            this.cmdGenerate.Name = "cmdGenerate";
            this.cmdGenerate.Size = new System.Drawing.Size(132, 44);
            this.cmdGenerate.TabIndex = 1;
            this.cmdGenerate.Text = "Genera";
            this.cmdGenerate.UseVisualStyleBackColor = true;
            this.cmdGenerate.Click += new System.EventHandler(this.cmdGenerate_Click);
            // 
            // cmdClose
            // 
            this.cmdClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdClose.Location = new System.Drawing.Point(12, 491);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Size = new System.Drawing.Size(183, 37);
            this.cmdClose.TabIndex = 2;
            this.cmdClose.Text = "Chiudi";
            this.cmdClose.UseVisualStyleBackColor = true;
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // lsbModelli
            // 
            this.lsbModelli.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lsbModelli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lsbModelli.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsbModelli.FormattingEnabled = true;
            this.lsbModelli.IntegralHeight = false;
            this.lsbModelli.ItemHeight = 16;
            this.lsbModelli.Location = new System.Drawing.Point(4, 32);
            this.lsbModelli.Name = "lsbModelli";
            this.lsbModelli.Size = new System.Drawing.Size(375, 450);
            this.lsbModelli.TabIndex = 3;
            // 
            // lsbSelezionati
            // 
            this.lsbSelezionati.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lsbSelezionati.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lsbSelezionati.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsbSelezionati.FormattingEnabled = true;
            this.lsbSelezionati.IntegralHeight = false;
            this.lsbSelezionati.ItemHeight = 16;
            this.lsbSelezionati.Location = new System.Drawing.Point(466, 32);
            this.lsbSelezionati.Name = "lsbSelezionati";
            this.lsbSelezionati.Size = new System.Drawing.Size(424, 151);
            this.lsbSelezionati.TabIndex = 4;
            this.lsbSelezionati.SelectedIndexChanged += new System.EventHandler(this.lsbSelezionati_SelectedIndexChanged);
            // 
            // cmdAdd
            // 
            this.cmdAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.cmdAdd.Location = new System.Drawing.Point(385, 40);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(75, 61);
            this.cmdAdd.TabIndex = 5;
            this.cmdAdd.Text = ">>";
            this.cmdAdd.UseVisualStyleBackColor = false;
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // cmdRemove
            // 
            this.cmdRemove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cmdRemove.Location = new System.Drawing.Point(385, 111);
            this.cmdRemove.Name = "cmdRemove";
            this.cmdRemove.Size = new System.Drawing.Size(75, 61);
            this.cmdRemove.TabIndex = 6;
            this.cmdRemove.Text = "<<";
            this.cmdRemove.UseVisualStyleBackColor = false;
            this.cmdRemove.Click += new System.EventHandler(this.cmdRemove_Click);
            // 
            // grbFirma
            // 
            this.grbFirma.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grbFirma.Controls.Add(this.rdbTutore);
            this.grbFirma.Controls.Add(this.rdbMadre);
            this.grbFirma.Controls.Add(this.rdbPadre);
            this.grbFirma.Controls.Add(this.rdbPaziente);
            this.grbFirma.Location = new System.Drawing.Point(896, 27);
            this.grbFirma.Name = "grbFirma";
            this.grbFirma.Size = new System.Drawing.Size(130, 156);
            this.grbFirma.TabIndex = 8;
            this.grbFirma.TabStop = false;
            this.grbFirma.Text = "Firma";
            // 
            // rdbTutore
            // 
            this.rdbTutore.AutoSize = true;
            this.rdbTutore.Location = new System.Drawing.Point(23, 124);
            this.rdbTutore.Name = "rdbTutore";
            this.rdbTutore.Size = new System.Drawing.Size(56, 17);
            this.rdbTutore.TabIndex = 3;
            this.rdbTutore.Text = "Tutore";
            this.rdbTutore.UseVisualStyleBackColor = true;
            this.rdbTutore.CheckedChanged += new System.EventHandler(this.rdbChiFirma_CheckedChanged);
            // 
            // rdbMadre
            // 
            this.rdbMadre.AutoSize = true;
            this.rdbMadre.Location = new System.Drawing.Point(23, 60);
            this.rdbMadre.Name = "rdbMadre";
            this.rdbMadre.Size = new System.Drawing.Size(55, 17);
            this.rdbMadre.TabIndex = 2;
            this.rdbMadre.Text = "Madre";
            this.rdbMadre.UseVisualStyleBackColor = true;
            this.rdbMadre.CheckedChanged += new System.EventHandler(this.rdbChiFirma_CheckedChanged);
            // 
            // rdbPadre
            // 
            this.rdbPadre.AutoSize = true;
            this.rdbPadre.Location = new System.Drawing.Point(23, 93);
            this.rdbPadre.Name = "rdbPadre";
            this.rdbPadre.Size = new System.Drawing.Size(53, 17);
            this.rdbPadre.TabIndex = 1;
            this.rdbPadre.Text = "Padre";
            this.rdbPadre.UseVisualStyleBackColor = true;
            this.rdbPadre.CheckedChanged += new System.EventHandler(this.rdbChiFirma_CheckedChanged);
            // 
            // rdbPaziente
            // 
            this.rdbPaziente.AutoSize = true;
            this.rdbPaziente.Checked = true;
            this.rdbPaziente.Location = new System.Drawing.Point(23, 28);
            this.rdbPaziente.Name = "rdbPaziente";
            this.rdbPaziente.Size = new System.Drawing.Size(66, 17);
            this.rdbPaziente.TabIndex = 0;
            this.rdbPaziente.TabStop = true;
            this.rdbPaziente.Text = "Paziente";
            this.rdbPaziente.UseVisualStyleBackColor = true;
            this.rdbPaziente.CheckedChanged += new System.EventHandler(this.rdbChiFirma_CheckedChanged);
            // 
            // tabDettagli
            // 
            this.tabDettagli.Location = new System.Drawing.Point(388, 186);
            this.tabDettagli.Name = "tabDettagli";
            this.tabDettagli.SelectedIndex = 0;
            this.tabDettagli.Size = new System.Drawing.Size(641, 295);
            this.tabDettagli.TabIndex = 9;
            // 
            // lblPaziente
            // 
            this.lblPaziente.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPaziente.Location = new System.Drawing.Point(8, 4);
            this.lblPaziente.Name = "lblPaziente";
            this.lblPaziente.Size = new System.Drawing.Size(882, 23);
            this.lblPaziente.TabIndex = 10;
            this.lblPaziente.Text = "Paziente: ";
            this.lblPaziente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1034, 536);
            this.Controls.Add(this.lblPaziente);
            this.Controls.Add(this.grbFirma);
            this.Controls.Add(this.cmdRemove);
            this.Controls.Add(this.cmdAdd);
            this.Controls.Add(this.lsbSelezionati);
            this.Controls.Add(this.lsbModelli);
            this.Controls.Add(this.cmdClose);
            this.Controls.Add(this.cmdGenerate);
            this.Controls.Add(this.tabDettagli);
            this.Name = "FormMain";
            this.grbFirma.ResumeLayout(false);
            this.grbFirma.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button cmdGenerate;
        private System.Windows.Forms.Button cmdClose;
        private System.Windows.Forms.ListBox lsbModelli;
        private System.Windows.Forms.ListBox lsbSelezionati;
        private System.Windows.Forms.Button cmdAdd;
        private System.Windows.Forms.Button cmdRemove;
        private System.Windows.Forms.GroupBox grbFirma;
        private System.Windows.Forms.RadioButton rdbTutore;
        private System.Windows.Forms.RadioButton rdbMadre;
        private System.Windows.Forms.RadioButton rdbPadre;
        private System.Windows.Forms.RadioButton rdbPaziente;
        private System.Windows.Forms.TabControl tabDettagli;
        private System.Windows.Forms.Label lblPaziente;
    }
}

