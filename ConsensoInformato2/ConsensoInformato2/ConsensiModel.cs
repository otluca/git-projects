﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ConsensoInformato2.Properties;
using System.Windows.Forms;
using ConsensoInformato2;
using System.Runtime.CompilerServices;
using Newtonsoft.Json.Linq;

namespace ConsensoInformato2
{
    public enum EChiFirma
    {
        Paziente,
        Padre,
        Madre,
        Tutore
    }


    public class TemplateDetail
    {
        public string Label { get; set; }
        public string Detail { get; set; }
    }


    public class ConsensoTemplate
    {
        public long Id { get; set; }
        public string FileLabel { get; set; }
        public string FileTemplatePath { get; set; }
        public List<TemplateDetail> TemplateSections { get; set; }

        internal void AddSections(List<JToken> templateSections)
        {
            this.TemplateSections = new List<TemplateDetail>();
            foreach (var section in templateSections) {
                this.TemplateSections.Add(new TemplateDetail() {
                    Label = section.ToString(),
                    Detail = string.Empty
                });
            }
        }
    }

    public class ConsensiModel
    {
        public List<ConsensoTemplate> ConsensoTemplates { get; private set; } = new List<ConsensoTemplate>();
        public List<ConsensoTemplate> ConsensoSelected { get; private set; } = new List<ConsensoTemplate>();

        public EChiFirma ChiFirma { get; set; } = EChiFirma.Paziente;


        public void LoadTemplates()
        {
            var id = 0;
            var listOfFiles = from x in Directory.EnumerateFiles(Common.TemplatesStore, $"*{Common.TemplatePostfix}.{Common.TemplateExtension}")
                              orderby Path.GetFileName(x).Replace(Common.TemplateExtension, string.Empty)
                              select new { 
                                  Path = x, 
                                  Label = Path.GetFileName(x)
                                          .Replace($"{Common.TemplatePostfix}.{Common.TemplateExtension}", string.Empty)};

            var idx = 0;
            foreach (var fileElement in listOfFiles) {
                ConsensoTemplates.Add(
                    new ConsensoTemplate() {
                        Id = idx++,
                        FileTemplatePath = fileElement.Path,
                        FileLabel = fileElement.Label,
                        TemplateSections = new List<TemplateDetail>()
                    });
            }

            var templatesConfigurationPath = Common.TemplatesStore + "\\Templates.json";
            if (File.Exists(templatesConfigurationPath)) {
                var content = File.ReadAllText(templatesConfigurationPath);
                JObject o = JObject.Parse(content);
                foreach (var item in o["templates"]) {
                    var templateFile = item["name"].ToString();
                    var templateSections = item["sections"].ToList();

                    if (templateSections.Count > 0) {
                        var consenso = (from x in ConsensoTemplates
                                        where x.FileTemplatePath.EndsWith(templateFile)
                                        select x)?.First();
                        if (consenso != null) {
                            consenso.AddSections(templateSections);
                        }
                    }
                }
            }
        }


        public void InsertConsenso(List<ConsensoTemplate>templates, ConsensoTemplate consenso)
        {
            int pos = 0;

            foreach(var item in templates) {
                if (item.Id > consenso.Id) {
                    templates.Insert(pos, consenso);
                    pos = -1;
                    break;
                }
                pos++;
            }

            if (pos >= 0) {
                templates.Add(consenso);
            }
        }

    }
}
